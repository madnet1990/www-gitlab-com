title: Sopra Steria
file_name: sopra_steria
cover_image: /images/blogimages/soprasteria.jpg
cover_title: |
  How GitLab became the cornerstone of digital enablement for Sopra Steria 
cover_description: |
  Sopra Steria’s digital transformation uses GitLab for operational efficiency, built-in security, and cloud-ready DevOps. 
canonical_path: /customers/sopra_steria/
twitter_image: /images/blogimages/soprasteria.jpg
twitter_text: Learn how Sopra Steria’s digital transformation uses GitLab for operational efficiency, built-in security, and cloud-ready DevOps.
customer_logo: /images/case_study_logos/sopra_steria-logo.wine.svg
customer_logo_css_class: brand-logo-tall
customer_industry: Technology
customer_location: Paris, France
customer_solution: GitLab Premium
customer_employees: 45,000 employees
customer_overview: |
   In the midst of a digital transformation, Sopra Steria adopted GitLab as their DevOps platform of choice.  
customer_challenge: |
  As teams started independently using Git for projects, Sopra Steria was searching for a Git platform to empower developers and accelerate software efficiency. 

key_benefits: |-
    Open source capabilities


    Single application for entire DevOps lifecycle


    Built-in security and compliance


    Improved quality of software


    Collaborative user experience


    Increased operational efficiencies

customer_stats:
  - stat: 10,000 
    label: GitLab users 
  - stat: 6   
    label: Minutes for a full software build 
  - stat: 0   
    label: Developer time spent setting up CI/CD
customer_study_content:
  - title: the customer
    subtitle: A distinguished IT services group
    content: >-

        Sopra Steria is a European leader in consulting, digital services, and software development. With over 50 years of experience, [Sopra Steria](https://www.soprasteria.com/) provides IT integrations and solutions, consulting services, application implementations, technical support, and various outsourcing operations for users and applications. Their broad customer base includes leading companies from  aerospace, government, financial services, retail, transportation, utilities, and insurance sectors. 
        
  - title: the challenge
    subtitle: Steadily evolving with Git
    content: >-

        Sopra Steria has had several years of success using Apache Subversion (SVN) and had experienced no standout problems with the software workarounds that were in place. However, the development team felt that they needed to evolve and embrace more innovation in order to create a more progressive and robust roadmap, keeping up with competitive technologies.   

        
        In 2017, several team members were using Git independently for various projects and it became clear that there was an internal need for a [Git platform](/blog/2020/04/20/ultimate-git-guide/). “I guess the main challenge was to make sure that inside the organization we had a full-fledged Git platform that was suitable for DevOps and that all the projects would use it because we were beginning to see projects massively moving from SVN to Git as the software management system,” said Yves Nicolas, Group CTO Office, Sopra Steria. The goal was to evolve before any outstanding challenges took place and move to a consolidated, cloud-ready DevOps tool.

        
        “It was really the fact that in the state of the software development we were in three years ago, with a group of 40,000 people, about half of them were committing code. We really wanted and we needed to have a proper unified accessibility for everybody in a Git managed software platform. That's what we wanted to achieve,” Nicolas said.

  - title: the solution
    subtitle: Operational efficiency, open source, and CI are priority
    content: >-

        The team wanted a Git platform that was available to anybody in the group with credentials using only their SSO from Sopra Steria. “It was the main requirement. The idea was that if you want to have access to one platform to share code and do some demo with usability, we want to test how it goes,” Nicolas said. “For that, it was more like testing, it was more demonstration and internal code. At the beginning, it was not necessarily for the code we provide to our customers or the code we develop for our customers.”   

        
        The teams did a comparison between Microsoft VSTS, GitHub, and GitLab. They quickly eliminated Microsoft VSTS because it was not possible to search code inside the environment. The ability to reuse code from one team to another was a necessity. .  

        
        GitLab was chosen over GitHub for three main reasons, however in no particular order. One reason was cost. “The entry point for GitLab enterprise was way below what GitHub was asking for GitHub enterprise at that time,” Nicolas said.

        
        The second reason was that GitLab has an [open source](/solutions/open-source/) version of its code and several groups already had installed a GitLab open source instance and were managing their source code with that. “We had a very good knowledge about GitLab administration inside the group. We felt a little more comfortable with that. It was easier to find people who could help us in setting up the project,” Nicolas said.

        
        The third reason was GitLab’s built-in continuous integration orchestrator. “I thought it was a very, very clever idea to have that directly inside the platform to have a complete environment where you could actually fully build in a continuous integrated way the software we are managing,” Nicolas said.

  - title: the results 
    subtitle: Happy developers, automated services, built-in security 
    content: >-

        Sopra Steria began adopting GitLab in March of 2017 and by June there were about 1,000 users. Today, there are over 9,000 team members connected. Developers are happy with this transition, which eased worry about potential turnover within the company. “One of our main challenges is keeping our best developers … We want to make sure they stay with a good environment. The fact that it was obvious that this was a good environment for them to stay and have good things to say was very well perceived by the business units, the people who are actually selling to our customers over vertical business units,” Nicolas said. 

        
        Sopra Steria includes over 40,000 people, so teams using GitLab vary from one extreme to the other. Some teams are very advanced in maturity around DevOps and software development. Teams working in our banking software edition group are using GitLab CI, working in microservices, using templates, and developing pipelines rapidly. While other teams have very little previous knowledge of Git and have only a small amount of continuous integration in their current projects. “What's interesting is that we have found that whatever the level of the team is at in the actual knowledge of these environments, GitLab is very user-friendly to use that,” Nicolas said. 

        
        The GitLab platform has helped to establish a digital transformation culture throughout Sopra Steria. “We have been able to have advanced teams work in advance and take the full-fledged advantage of the GitLab CI, and then spread their knowledge so that all the teams, even the less advanced ones which are kind of new to Git migrating their projects from SVN to Git can take advantage right from now with the GitLab CI environments. It is really impressive,” Nicolas said.

        
        GitLab’s flexible hosting options allows Sopra Steria’s large network to use a variety of cloud integrations, including Azure, AWS, and GCP. “This is kind of transparent. The way it clogs into the cloud environment is that from the GitLab instance and using the continuous integration and continuous deployment pipelines, we are deploying from GitLab to any kind of cloud at any kind of level,” Nicolas said.

        
        Prior to GitLab, security checks were done manually, requiring time and effort. [Security testing](/blog/2019/08/12/developer-intro-sast-dast/) is now implemented in project templates and no longer needs manual effort for each and every test. “They don’t have to worry anymore about how to set up a test -- this security test makes them use it.  Sometimes they would've even skipped some tests.” Nicolas said. “Because it's easier for the user now, they have taken it for granted. It's no effort for the team, so that results in better quality software.” 

        
        Developers have gone from having to set up their own planning processes and services that they need to roll out within their software projects, to now having these steps already formatted for them. GitLab has simplified their overall workflow and improved collaboration between teams. According to Nicolas, “It is very often confirmed by the teams that using GitLab has significantly reduced the time they needed for both setup and managing the whole software build environment and the continuous integration chain. This reduction is probably the one that is the best thing to the bottom line.” 

        
        Sopra Steria and GitLab have had a successful working relationship for about three years. With GitLab’s release updates, Sopra Steria continues to appreciate the unified platform. “It has been confirmed over the years with the inclusion of all the DevOps related features that GitLab is a unique platform for every DevOps tools in some way.”
customer_study_quotes:
  - blockquote: Going into full automated DevOps is key for a software group like ours. Our GitLab instance is the cornerstone of what we call the digital enablement platform internally, which is really the means to enable our team to do cloud native development in a full automated DevOps mode.
    attribution: Yves Nicolas 
    attribution_title: Group CTO Office, Sopra Steria
more_case_studies:
  - title: Worldline improves code reviews’ potential by 120x
    href: /customers/worldline/
  - title: The U.S. Army Cyber School created secure, collaborative coursework with
      GitLab CI/CD, DevOps, and SCM.
    href: /customers/us_army_cyber_school/  
