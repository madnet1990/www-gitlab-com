---
layout: handbook-page-toc
title: "Application Security Runbooks"
---

## On this page
{:.no_toc .hidden-md .hidden-lg}

- TOC
{:toc .hidden-md .hidden-lg}

## Application Security Runbooks

- [HackerOne Process][1]
- [Procedures for Handling severity::1/priority::1 Issues][2]
- [Security Engineer Process for both Regular and Critical Security Releases][5]
- [Verifying Security Fixes]
- [Security Dashboard Review][3]
- [Triage Rotation][4]
- [AppSec Review Template Process]
- [Threat Modeling]
- [Investigating Package Hunter Findings]
- [Release Certification Process](/handbook/ceo/chief-of-staff-team/jihu-support/release-certification.html)
- [JiHu Contribution Security Review Process](/handbook/ceo/chief-of-staff-team/jihu-support/jihu-contribution-review-process.html)
- [AppSec Engineer's Local Setup][6]
- [Federal AppSec Container Scanning Review](./federal-appsec-container-scanning-review.html)
- [JiHu Contribution Merge Monitor Report Process](./jihu-contribution-merge-monitor-reports.html)
- [Bug Hunting Day process][8]
- [Appsec Frequenty Asked Questions][7]
- [How to plan and measure engagement with Secure Code Warrior][9]
- [Holiday and Friends and Family day coverage](./holiday-coverage.html)
- [Gem Review Guidelines for AppSec Engineers][10]
- [FedRAMP vulnerabilities triage runbook and guidance][11]
- [How to handle upstream security patches?][12]
- [Working with SIRT][13]

## Note for New team members

Whenever you are on a rotation ([HackerOne][1] or [Triage Rotation][4]) or doing your onboarding process and need help or advice, reach out in the `#sec-appsec` Slack channel or ask during an AppSec Sync meeting. Here are some examples on scenarios where you may need ask or need help:

- You're doing your onboarding tasks, threat modeling, or appsec reviews, and you're stuck on it; or don't know how to tackle something in particular
- You're on ping rotation and you don't know how to deal with a particular situation or what to do with a specific question
- You're on HackerOne rotation and have to deal with a hard report

[1]: ./hackerone-process.html
[2]: ./handling-s1p1.html
[3]: ./security-dashboard-review.html
[4]: ./triage-rotation.html
[5]: https://gitlab.com/gitlab-org/release/docs/-/blob/master/general/security/security-engineer.md
[6]: ./local-setup.html
[7]: ./faq.html
[8]: ./bug-hunting-day.html
[9]: ./scw-engagement-plan.html
[10]: ./gem-review-guidelines.html
[11]: ./fedramp-scanners-process.html
[12]: https://gitlab.com/gitlab-org/release/docs/-/blob/master/general/security/runbooks/upstream_security_patches.md
[13]: ./working-with-sirt.html
[Verifying Security Fixes]: ./verifying-security-fixes.html
[AppSec Review Template Process]: ./review-process.html
[Investigating Package Hunter Findings]: ./investigating-package-hunter-findings.html
[Threat Modeling]: ./threat-modeling.html
