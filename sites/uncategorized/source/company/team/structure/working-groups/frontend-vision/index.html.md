---
layout: markdown_page
title: "Frontend Vision Group"
description: "Write guidelines about frontend architecture practices we want to have on GitLab frontend in three years"
canonical_path: "/company/team/structure/working-groups/frontend-vision/"
---

## On this page

{:.no_toc}

- TOC
  {:toc}

## Attributes

| Property          | Value                                                                                                                                     |
| ----------------- | ----------------------------------------------------------------------------------------------------------------------------------------- |
| Date Created      | 2023-03-27                                                                                                                                |
| Target End Date   | TBD                                                                                                                                       |
| Slack             | #wg_frontend-vision (only accessible from within the company)                                                                             |
| Google Doc        | [Agenda](https://docs.google.com/document/d/1H0td5YZJROQG_aOkGJuIpXqxi4UEpiuCufvEAc27kMo/edit?usp=sharing) (only accessible from within the company) |
| Epic              |                                                                               |
| Overview & Status | See [Exit Criteria Progress](#exit-criteria-progress)                                                                                     |

### Context

Currently, our frontend development guidelines only document features and practices _we are currently using in production_. There is no clear documentation about what we see as a desired state of the frontend at GitLab, nor guidelines regarding practices we plan to deprecate. It would be nice to define a set of high-level expectations regarding frontend architecture, tech stack, and best practices as we see them in the scope of three years from now. Here is a non-exhaustive list of topics we should discuss:

- Single-Page Application vs Multi-Page Application vs hybrid approach (multiple SPA's that handle some functionality).
- Server-Side rendering
- Technical stack (frontend framework, state management approach, testing levels and tools)
- Real-time features, optimistic updates, frontend caching and "snappy GitLab experience" overall
- Exception process when certain group requires the tool that deviates from the main strategy

### Exit Criteria

This Working Group has the following goals:

1. Discuss and come to conclusion regarding the frontend vision
1. Add the documentation about frontend vision to frontend development guidelines via a set of merge requests
1. Develop and implement a communication plan for the outcomes of the working group.
1. Document the process for proposing changes to the main strategy.

#### Exit Criteria Progress

| Criteria                                                                                      | Start Date | Completed Date | Progress | DRI     |
| --------------------------------------------------------------------------------------------- | ---------- | -------------- | -------- | ------- |
| Add guidelines about frontend vision                                                          |            |                | 0%       | `@you?` |

### Roles and Responsibilities

| Working Group Role | Person                | Title                                                        |
| ------------------ | --------------------- | ------------------------------------------------------------ |
| Executive Sponsor  | Christopher Lefelhocz | VP of Development (Delegating to Tim Zallmann)               | 
| Executive Sponsor  | Tim Zallmann          | Senior Director of Engineering                               |
| Facilitator        | Donald Cook           | Engineering Manager, Plan:Project Management                 |
| Functional Lead    | Natalia Tepluhina     | Principal Engineer, Plan                                     |
| Member             | Vitaly Slobodin       | Staff Frontend Engineer, Fulfillment                         |
| Member             | Paul Slaughter        | Staff Frontend Engineer, Create:Editor                       |
| Member             | Mark Florian          | Senior Frontend Engineer, Ecosystem:Foundations              |
| Member             | Lukas Eipert          | Senior Frontend Engineer, Ecosystem:Foundations              |
| Member             | Frédéric Caplette     | Senior Frontend Engineer, Verify:Pipeline Authoring         |
| Member             | Andrei Zubov          | Senior Frontend Engineer, Release                            |
| Member             | Stanislav Lashmanov   | Senior Frontend Engineer, Create:Source Code                 |
