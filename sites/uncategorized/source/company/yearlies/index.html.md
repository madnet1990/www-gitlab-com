---
layout: handbook-page-toc
title: "Yearlies"
description: "Yearlies are the annual goals for the company. Yearlies should have measurable deliverables."
---

## On this page
{:.no_toc .hidden-md .hidden-lg}

- TOC
{:toc .hidden-md .hidden-lg}

## Yearlies

Yearlies are the annual goals for the company. Yearlies should have measurable deliverables.

## Alignment 

Yearlies connect our [3 year strategy](/company/strategy/) to our shorter-term quarterly objectives (OKRs). Achieving our Yearlies creates progress towards achieving our [strategy](/company/strategy/), and as a result, moves us closer to achieving our [vision](/company/vision/).

### Three Year Strategy 

Yearlies are informed by the [three-year strategy](/company/strategy/). We update them on an annual [cadence](/company/cadence/). 

### Annual Plan and Yearlies

1. Yearlies come before the [Annual Plan](/handbook/finance/financial-planning-and-analysis/#plan). 
1. Yearlies contain our priorities for the fiscal year while the Annual Plan contains our budgets and our financials.  
1. We first determine our priorities for the upcoming year in the form of Yearlies, then we use these priorities to inform the budget in the Annual Plan process. 

The Annual Plan process [finishes two quarters after](https://about.gitlab.com/company/offsite/#offsite-topic-calendar) Yearlies are finalized. As a result, Yearlies may become outdated since being established. In line with our value of [Iteration](https://about.gitlab.com/handbook/values/#iteration), Yearlies are [reviewed each E-Group offsite](https://about.gitlab.com/company/offsite/#recurring-discussion-topics) and updated as needed. Annual Plan and Yearlies should be synced when Annual Plan is finalized.

### Objectives and Key Results (OKRs)

1. [Objectives and Key Results (OKRs)](/company/okrs/) are our quarterly priorities that create progress for our yearly goals.
1. As a result, [OKRs](/company/okrs/)) are aligned to one of the yearlies. 
1. While [OKRs](/company/okrs/) are not directly aligned to one of the three pillars of the [three year strategy](/strategy/#three-year-strategy), since OKRs are aligned to one of the Yearlies, and the Yearlies are aligned to our strategic pillars, OKRs are indirectly aligned to GitLab's strategy.
1. [OKRs](/company/okrs/) have a duration of one quarter while Yearlies are annual goals with a duration of a year.
1. [OKRs](/company/okrs/) are composed of Objectives and Key Results. Yearlies have an annual goal and a few key supporting goals and initiatives. [Top Cross-Functional Iniative](/company/top-cross-functional-initiatives/) exit outcomes will be listed among supporting goals. 

## Cadence

1. The [three year strategy](/company/strategy/#three-year-strategy) is on a 3 year cadence and is inspiration for the Yearlies, which are on a 1 year [cadence](/company/cadence/#year). The three year strategy is reviewed as part of [E-Group offsite calendar](/company/offsite/#offsite-topic-calendar). 
1. Yearlies are established during E-Group offsite as a once-a-year topic in the [offsite topic calendar](https://about.gitlab.com/company/offsite/#offsite-topic-calendar). 
1. Yearlies are the company's goals for the next 12 months unless the Yearly is achieved sooner than 12 months or E-Group decides that a Yearly should be updated. 
1. In line with our value of [Iteration](https://about.gitlab.com/handbook/values/#iteration), Yearlies are reviewed during each E-Group offsite as a [recurring discussion topic](/company/offsite/#recurring-discussion-topics) and updated or replaced if the Yearly needs to be changed sooner than 12 months after being established. 

## FY24 Yearlies

FY24 Yearlies and additional detail can be found by [searching for the 5 FY24 Company Objectives Google Doc](https://docs.google.com/document/d/1epErkkHT__-UPyex0DDiAOw-y7NcyzGap0ttRfgRoak/edit?usp=sharing). Initiatives in bold and hyperlinked are part of GitLab's existing [Top Cross-Functional Initiatives](/company/top-cross-functional-initiatives). 

1. Deliver predictable high value to customers. Reduce churn and contraction from ~X% ATR to ~Y% ATR / quarter
   1. \>95% of customer ARR with [**usage reporting**](/company/top-cross-functional-initiatives/#current-top-cross-functional-initiatives)
   1. Recognize and incentivize organizations who are among our most active contributors. Our goal is for 10% of GitLab customers to be [**Leading Organizations**](/company/top-cross-functional-initiatives/#current-top-cross-functional-initiatives) by FY25-Q4
1. Drive Ultimate
    1. Cross functional program to drive [**DevSecOps Adoption**](/company/top-cross-functional-initiatives/#current-top-cross-functional-initiatives) with Ultimate Net ARR Bookings from $xM in FY23 to $yM in FY24 including shipping top product priorities in (Security, Plan, Value Stream Analytics)
1. Make GitLab easier to do business with
    1. Customers can get an order form, place the order and have active licenses within 4 hours 
    1. % of sales assisted orders with license key errors is below 0.5%
    1. Deliver against key [**Fulfillment Efficiency**](/company/top-cross-functional-initiatives/#current-top-cross-functional-initiatives) projects. Includes shipping temporary licenses capability 
    1. Achieve [**FedRAMP**](/company/top-cross-functional-initiatives/#current-top-cross-functional-initiatives) moderate certification for a GitLab Dedicated federal SaaS offering
    1. Onboard at least 5 customers to [**Dedicated**](/company/top-cross-functional-initiatives/#current-top-cross-functional-initiatives) per quarter
    1. Expand GitLab's access to [**AWS/GCP**](/company/top-cross-functional-initiatives/#current-top-cross-functional-initiatives) LAM via cloud-first sales plays, new products & solutions, lower friction buying, reduced sales ops costs
    1. Roll out [**consumption add-on**](/company/top-cross-functional-initiatives/#current-top-cross-functional-initiatives). Specifically, launch Ghidorah and ModelOps add-on
1. Continue to build a diverse team of top talent that we retain and grow 
    1. Regrettable turnover from x% to below y%
    1. Increase our URG population from 17% to 19% 
    1. Increase our female population from 31% to 34%
    1. Increase leadership Elevate graduates from 10% to 85% of our people managers
1. AI in all we do
    1. Make GitLab (the application) smarter by shipping 16 AI/ML-assisted features and workflows across developer, security, and operations personas and launching AI messaging to support GitLab’s “AI in all we do” focus


