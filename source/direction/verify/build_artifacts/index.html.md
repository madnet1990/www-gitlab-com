---
layout: markdown_page
title: "Category Direction - Build Artifacts"
description: "Usage and administration of Build Artifacts"
canonical_path: "/direction/verify/build_artifacts/"
---

- TOC
{:toc}

## Build Artifacts

[Artifacts](https://docs.gitlab.com/ee/ci/yaml/#artifacts) are files created as part of a build process that often contain metadata about that build's jobs like test results, security scans, etc. These can be used for reports that are displayed directly in GitLab or can be published to [GitLab Pages](/stages-devops-lifecycle/pages/) or in some other way for users to review. These artifacts can provide a wealth of knowledge to development teams and the users they support.

[Job Artifacts](https://docs.gitlab.com/ee/ci/pipelines/job_artifacts.html) and [Pipeline Artifacts](https://docs.gitlab.com/ee/ci/pipelines/pipeline_artifacts.html) are both included in the scope of Build Artifacts to empower GitLab CI users to more effectively manage testing capabilities across their software lifecycle in both the gitlab-ci.yml or as the latest output of a job.

For information about storing containers or packages or information about release evidence see the [Package Stage direction page](https://about.gitlab.com/handbook/product/categories/#package-stage).

## Additional Resources

- [Issue List](https://gitlab.com/groups/gitlab-org/-/issues?scope=all&state=opened&label_name[]=Category%3ABuild%20Artifacts)
- [Overall Vision of the Verify stage](/direction/ops/#verify)

For specific information and features related to display of artifact data, check out the [GitLab Features](/features/) and for information about administration of artifacts please reference the Job Artifact [documentation](https://docs.gitlab.com/ee/administration/job_artifacts.html). You may also be looking for one of the related direction pages from the [Verify Stage](/direction/ops/#verify-stage-categories).

Interested in joining the conversation for this category? Please join us in the [issues](https://gitlab.com/gitlab-org/gitlab/-/issues/?sort=popularity&state=opened&label_name%5B%5D=Category%3ABuild%20Artifacts&first_page_size=20) where we discuss this topic and can answer any questions you may have. Your contributions are more than welcome.

This page is maintained by the Product Manager for Pipeline Security, Jocelyn Eillis ([E-mail](mailto:jeillis@gitlab.com)).

## What's Next & Why

Our team is committed to release of our Artifacts page MVC [gitlab#33418](https://gitlab.com/gitlab-org/gitlab/-/issues/33418) to enable a better [artifact management experience](https://gitlab.com/groups/gitlab-org/-/epics/8311). We are completing the work to enable [ability to bulk delete artifacts](https://gitlab.com/gitlab-org/gitlab/-/issues/33348) - enabling a feature set allowing users to perform storage management via UI. 

Beyond this, there are no additionally planned features in Build Artifacts in FY24 (2023-02 to 2024-01), however, Pipeline Security will support high priority bug fixes for this category during this time. 

## Top Customer Success/Sales Issue(s)

The Gitlab Sales teams are looking for more complex ways for customers to make use of Ultimate and Premium features like SAST and DAST with monorepos by letting customers [namespace parts of reports](https://gitlab.com/gitlab-org/gitlab/-/issues/299490) to more granular analysis or combining Matrix Builds and [Metrics Reports](https://gitlab.com/gitlab-org/gitlab/-/issues/10788).

## Top Customer Issue(s)

The most popular customer request is for the ability to support the [generation of multiple artifacts per job](https://gitlab.com/gitlab-org/gitlab/-/issues/18744) to reduce the need for pipeline logic to make select files available to later jobs.

Another popular customer request is the ability to [reference child pipelines from the parent pipeline](https://gitlab.com/gitlab-org/gitlab/-/issues/285100). Visibility/Traceability and [seamless artifact handling for parent/child pipelines](https://gitlab.com/groups/gitlab-org/-/epics/4019) is a recurring usability theme we have heard from our customers.  

One of our most complicated request, is to handle the [expire_at](https://gitlab.com/groups/gitlab-org/-/epics/7097) experience in self-managed customers better. Today, our implementation deletes data for both GitLab.com and self-managed users - rather than allowing more control for our self-managed customers. 

Although we have made improvements to expiration of artifacts, we continue to see customer struggles with reliability for removal of these expired artfacts and ensuring [cleanup methods are removing all items](https://gitlab.com/groups/gitlab-org/-/epics/9343).  

## Top Internal Customer Issue(s)

The Gitlab quality team has requested the ability to upload artifacts from a job when it [fails due to a timeout](https://gitlab.com/gitlab-org/gitlab/-/issues/19818) to assist in debugging those pipeline failures.

The team is also investigating performance issues related to the build artifact feature set as part of our focus on [Availability and Performance](/direction/enablement/dotcom/#availability-and-performance).

## Top Vision Items

To meet our long term vision that enables users to more easily use and manage their Build Artifacts we will need to add and improve the [usability of artifact in the UI](https://gitlab.com/groups/gitlab-org/-/epics/8311). In addition to UI improvements, we need to provide [user-friendly solutions for build artifacts management](https://gitlab.com/groups/gitlab-org/-/epics/8715) via API, for example, [an API to upload artifacts](https://gitlab.com/gitlab-org/gitlab/-/issues/18794) directly to GitLab without them being generated by a pipeline. 
